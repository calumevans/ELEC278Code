#include <stdio.h>

int main (void)
{
	int		x = -1;
    int     y = -1;
    int		**pp;
    int		*pi[2];

    pi[0] = &x;
    pi[1] = &y;
    *pi[0] = 4;
    *pi[1] = 5;
    printf ("A:  %d  %d\n",  x, y);
	pp = pi;    // HINT: same as pp=&pi[0];

	**pp++ = 90;
	**pp   = 75;
	pi[0]  = &y;
	**--pp = 35;
	printf ("B:  %d  %d\n",  x, y);

	return 0;
}
