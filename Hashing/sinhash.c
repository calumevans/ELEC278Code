#include <stdio.h>


#define  odd(x)	(x & 1)

unsigned int hash_function (unsigned int key)
{
	unsigned int	answer = 0;
	int				i;
	for (i=0; i<9; i++)	{
		answer = answer + (odd(i)?2:1)*(key%10);
		key = key/10;
		}
	answer = answer % 100;
	return answer;
}


int main(void)
{
	unsigned int	sin;
    unsigned int	hashvalue;
	char			buff [128];

	while (1)	{
		printf ("Enter 9 digit number: ");
		gets (buff);
		sin = atoi(buff);
		if (sin == 0)	break;
		if (sin > 100000000  && sin < 999999999)	{
			// valid number
			hashvalue = hash_function (sin);
			printf ("Hash value: %d\n", hashvalue);
		} else	{
			printf ("Illegal number\n");
			}
		}
	return 0;
}
