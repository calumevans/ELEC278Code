// Fibonnacci calculation

#include <stdio.h>
#include <math.h>

int   g = 0;         // global counter - used by fib.
int   g2 = 0;        // counter used by fib2


int fib (int n)
{
    // Another call - increment global counter
    g++;
    if (n<=1)  return n;
    return fib(n-1)+fib(n-2);
}


int values [50];

int fib2 (int n)
{
    int  t1, t2;
    // Another call - increment global counter
    g2++;
    if (n<=1)  {
        values[n] = n;
        return n;
        }
    t2 = values[n-2] ? values[n-2] : fib2 (n-2);
    t1 = values[n-1] ? values[n-1] : fib2 (n-1);
    return t1+t2;
}

int main (void)
{
    int i, k, fibi, fibi2, orderfibi;
    float  pw;

    printf ("   N   FIB(N) Calls to Fib FIB2(N) Calls to Fib2  1.6^N     /g        /g2\n");
    for (i=0; i<20; i++)   {
        g = 0;
        g2 = 0;
        for (k=0; k<50; k++)    values[k] = 0;
        values[1]= 1;

        fibi = fib (i);
        fibi2 = fib2 (i);
        pw = pow(1.6, i);
        printf("%5d%6d%10d%12d%12d%10.1f%9.2f%9.2f\n", i, fibi, g, fibi2, g2, pw, pw/g, pw/g2);
        }
    return 0;
}