// GRAPH.H
// Implementation of basic graph functions. Handles both directed
// and non-directed graphs.
// This version creates graph data structures dynamically.

#include "boolean.h"

// Graph vertex contains both vertex label and flag if visited. Visit
// flag used for traversal
typedef struct {
	char label;
	BOOL visited;
	} Vertex;


typedef struct	{
	int		vertexcount;	// number of vertices
	Vertex	*Vertices;		// pointer to array of Vertices
	int		*adjMatrix;		// pointer to array of vertexcount*vertexcount ints
	} Graph, pGraph;


static void zero (int *pa, int size)
// Fill memory with size zeros, starting at a.
{
	while (size--)	*pa++ = 0;
}//zero()


pGraph	createGraph (int size)
// Create graph with size vertices
{
	pGraph	pg = NULL;

	// start with idiot check
	if (size > 0)	{
		int	sizesquared = size*size;
		// allocate space for Vertex array
		Vertex *pv = (Vertex *) malloc (size * sizeof (Vertex));
		// and space for adjacency array
		int *pa = (int *) malloc (sizesquared * sizeof (int));
		// did we get space?
		if (pv == NULL || pa == NULL)	{
			// at least one failed - give up whatever memory obtained
			if (pv)	free (pv);
			if (pa)	free (pa);
		} else	{
			// Got actual memory for storing graph information
			pg = (pGraph) malloc (sizeof (Graph));
			if (pg)	{
				pg->vertexcount = size;
				pg->Vertices = pv;
				zero (pg, size);		// DANGER! Assumes pointer same size
										// as integer.
				pg->adjMatrix = pa;
				zero (pa, sizesquared);
				}//endif got Graph descriptor memory
			}//endif got Vertex and Edge table memory
		}//endif size request made sense
	return pg;
//createGraph();


void eraseVertices (pGraph pg)
// Delete memory associated with vertex storage
{
	if (pg)	{
		for (i=0; i<pg->vertexcount; i++)	{
			if (pg->Vertices[i])	{
				free (pg->Vertices[i]);
				pg->Vertices[i] = NULL;
				}
			}
		}
}//eraseVertices()


void destroyGraph (pGraph *pg)
// Destroy existing graph structure. Note that parameter is pointer to graph
// pointer, so last step can be to zero caller's graph pointer.
{
	// always start with sanity check
	if (pg)	{
		pGraph rpg = *pg;		// get copy of real graph pointer
		// get rid of any Vertex memory
		eraseVertices (pg);
		if (rpg->Vertices)	free (rpg->Vertices);
		if (rpg->adjMatrix)	free (rpg->adjMatrix);
		free (rpg);
		*pg = NULL;				// graph pointer zeroed
		}
}//destroyGraph()


// Graph initialization functions

void zeroGraphEdges (pGraph pg)
// Initialize adjacency matrix in graph data structure to have no edges.
{
	if (pg)
		zero (pg->adjMatrix, pg->vertexcount * pg->vertexcount);
}//zeroGraphEdges()


void resetVisitFlags (pGraph pg)
// Reset visited flag in all vertices
{
	if (pg)	{
		int		i;
		for (i=0; i<pg->vertexCount; i++)
			pg->Vertices[i]->visited = FALSE;
		}
}//resetVisitFlags()


// Graph configuration functions

void addVertex(pGraph pg, char label)
// Add vertice with parameter label to vertex list
{
	if (pg)	{
		Vertex* vertex = (Vertex*) malloc(sizeof(Vertex));
		vertex->label = label;  
		vertex->visited = FALSE;     
		pg->Vertices[vertexCount++] = vertex;
		}
}


void addEdge(pGraph pg, int start, int end)
// Add edge to edge array in a non-directed graph.
{
	if (pg)	{
		int	ndx = end + start * pg->vertexCount;
		pg->adjMatrix[ndx] = 1;
		ndx = start + end * pg->vertexCount;
		pg->adjMatrix[ndx] = 1;
		}
}//addEdge()


void addDirectedEdge (pGraph pg, int start, int end)
// Add directed edge from start to end in edge array
{
	if (pg)	{
		int ndx = end + start * pg->vertexCount;
		adjMatrix[ndx] = 1;
		}
}//addDirectedEdge()


void addWeightedEdge (pGraph pg, int start, int end, int weight)
// Add edge to edge array with weight
{
	if (pg)	{
		int	ndx = end + start * pg->vertexCount;
		pg->adjMatrix[ndx] = weigh;
		ndx = start + end * pg->vertexCount;
		pg->adjMatrix[ndx] = weigh;
		}
}//addWeightedEdge()


void addWeightedDirectedEdge (pGraph pg, int start, int end, int weight)
// Add directed edge from start to end in edge array
{
	if (pg)	{
		pg->adjMatrix[end + start*pg->vertexcount] = weight;
		}
}//addWeightedDirectedEdge()


void displayVertex(pGraph pg, int vertexIndex)
// Display vertex label
{
	printf("%c ",pg->Vertices[vertexIndex]->label);
}//displayVertex()       


int getAdjUnvisitedVertex(pGraph pg,int vertexIndex)
// Go through adjacency matrix vertexIndex row to find neighbours that have
// not yet been visited.

// TBA

{
   int i;
   for(i=0; i<vertexCount; i++) {
      if(adjMatrix[vertexIndex][i] == 1 && (!Vertices[i]->visited)) {
         return i;
      }
   }        
   return -1;
}
