// GRPHBSIC.H
// Implementation of basic graph functions. Handles both directed
// and non-directed graphs.
// This version creates single
// global data structure for graph.  Prior to including this header,
// the following define is required:
//	#define MAX 7
//	#include "grphbsic.h"

#include "boolean.h"

// Graph vertex contains both vertex label and flag if visited. Visit
// flag used for traversal
typedef struct {
	char label;
	BOOL visited;
	} Vertex;

// graph variables
Vertex* Vertices[MAX];		// array of pointers to vertices
int		adjMatrix[MAX][MAX];// effectively contains simple edge
							// information
int		vertexCount = 0;	// vertex count


// Basic graph functions

void initGraph (void)
// Initialize adjacency matrix to have no edges
{
	int		i, j;
	for(i=0; i<MAX; i++)
		for(j=0; j<MAX; j++)
			adjMatrix[i][j] = 0;
}

void resetVisitFlags (void)
// Reset visited flag in all vertices
{
	int	i;
	for(i=0;i<vertexCount;i++)	Vertices[i]->visited = FALSE;
}


void addVertex(char label)
// Add vertice with parameter label to vertex list
{
	Vertex* vertex = (Vertex*) malloc(sizeof(Vertex));
	vertex->label = label;  
	vertex->visited = FALSE;     
	Vertices[vertexCount++] = vertex;
}


void addEdge(int start,int end)
// Add edge to edge array
{
	adjMatrix[start][end] = 1;
	adjMatrix[end][start] = 1;
}


void addDirectedEdge (int start, int end)
// Add directed edge from start to end in edge array
{
	adjMatrix[start][end] = 1;
}


void addWeightedEdge (int start, int end, int weight)
// Add edge to edge array with weight
{
	adjMatrix[start][end] = weight;
	adjMatrix[end][start] = weight;
}


void addWeightedDirectedEdge (int start, int end, int weight)
// Add directed edge from start to end in edge array
{
	adjMatrix[start][end] = weight;
}


void displayVertex(int vertexIndex)
// Display vertex label
{
	printf("%c ",Vertices[vertexIndex]->label);
}       


int getAdjUnvisitedVertex(int vertexIndex)
// Go through adjacency matrix vertexIndex row to find neighbours that have
// not yet been visited.
{
   int i;
   for(i=0; i<vertexCount; i++) {
      if(adjMatrix[vertexIndex][i] == 1 && (!Vertices[i]->visited)) {
         return i;
      }
   }        
   return -1;
}
