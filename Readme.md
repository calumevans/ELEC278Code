Source code used for teaching purposes in
   ELEC278, Queen's University, Kingston, Winter semester 2018.
This code is provided without warranty of any kind.  It is the responsibility
of the user to determine the correctness and the usefulness of this code.

Author:  David F. Athersych
All rights reserved.

See LICENCE.MD for restrictions on the use of this code.
