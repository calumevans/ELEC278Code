echodate "Comparing to OneDrive archive"
set DIR=ELEC278\Lectures\Code
if exist \Users\David\SkyDrive\nul  (
    cmpdir -r \courses\%DIR%  \Users\David\Skydrive\courses\%DIR% > \tmp\278Code.txt
    goto duncmp
    )
if exist \Users\David\OneDrive\nul  (
    cmpdir -r \courses\%DIR%  \Users\David\OneDrive\courses\%DIR% > \tmp\278Code.txt
    goto duncmp
    )
:duncmp
pause
notepad \tmp\278Code.txt
pause