// LNKLST1.C [160928]
//
// Code provided for ELEC278
//
// This code implements linked lists. It is not a particularly good
// implementation - it is intended only to show basic linked list
// manipulation.
//
// In this version, a pointer to a list element is used as the pointer
// to the list.  This is a different implementation than found in Lab 2.

/* * START LICENSE
Code developed for educational purposes only.

Copyright 2016, 2017 by
David F. Athersych, Kingston, Ontario, Canada. (THE AUTHOR).
This software may be included in systems delivered or distributed by
Cynosure Computer Technologies Incorporated, Kingston, Ontario, Canada.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted, provided
that the above copyright notice appears in all copies and that both the
above copyright notice and this permission notice appear in supporting
documentation.  This software is made available "as is", and

THE AUTHOR DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, WITH REGARD
TO THIS SOFTWARE, INCLUDING WITHOUT LIMITATION ALL IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND IN NO EVENT
SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, TORT (INCLUDING NEGLIGENCE)
OR STRICT LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

For more information, see www.cynosurecomputer.ca
* END LICENSE */

#include <stdio.h>
#include <stdlib.h>


// Node (or element) in list holds value and pointer to next element.
// Definition of structure comes first; typedef makes typing easier.
struct node {
	struct node	*next;
	int			key;
	int			value;
};
typedef struct node	Node;


Node *CreateNode (int ky, int val, Node *nxt)
// Create new node structure and initialize fields.
// Returns pointer to new node created, or NULL if malloc() failed to
// find memory.
{
	Node *pNode = (Node *)malloc (sizeof(Node));
	if (pNode != NULL)	{
		pNode->next = nxt;
		pNode->key = ky;
		pNode->value = val;
		}
	return pNode;
}//createNode()


void DestroyNode (Node *pnode)
// Basically a pretty way of using free()
{
	free (pnode);
}//DestroyNode()


int isEmptyList (Node *listhead)
// Empty list will have its head pointer not pointing to anything
{
	return  (listhead==NULL);
}//isEmptyList()


void PrintAllNodeData(Node *listhead)
// Print all node data for Nodes found in list pointed to by listhead
// NOTE: This function takes a pointer to the first node in the list. Contrast
// this with functions that change the state of the list - those routines
// take a pointer to the pointer to the first node in the list.
{
	if (listhead != NULL)	{
		// Data to print
		while (listhead != NULL)	{
			printf ("key: %d, value: %d\n", listhead->key, listhead->value);
			listhead = listhead->next;
			}
	} else	{
		printf ("EMPTY LIST\n");
		}
}//PrintAllNodeData()


void AddNodeToFront(Node **plisthead, Node *newnode)
// Code to add new node to front of existing list.
// Parameters:
//	listhead - points to existing linked list head pointer
//	newnode - points to new node which is to be added (see createNode())
{
	Node	*tmp;		// copy of pointer to first node in list
	tmp = *plisthead;	// point to first node in list
	// Node pointed to by newnode will become first node in list. Existing
	// list may have Nodes, in which case head of list points to first one.
	// Existing list may be empty, and head of list contains NULL.
	newnode->next = tmp;
	*plisthead = newnode;
} //AddNodeToFront()	


void AddNodeToEnd (Node **plisthead, Node *newnode)
// Add new node to end of existing list.  In order to do this, have to find
// last node in the list.  In this implementation, that means traversing list
// until we come to the end.  How do we know when we've come to the end? Node
// we're looking at has a NULL next pointer.
{
	Node	*tmp = *plisthead;	// point to first node in list

	if (tmp == NULL)	{
		// list currently empty - new node becomes first node in list
		*plisthead = newnode;
	} else	{
		// non-empty list.  Find end of it.
		while (tmp->next != NULL)	tmp = tmp->next;
		// tmp points to node with NULL next field - must be last node in
		// list.
		tmp->next = newnode;
		}
}//AddNodeToEnd()


void RemoveNodeFromFront (Node **plisthead)
// Remove first node from list
{
	Node	*tmp = *plisthead;	// point to first node in list

	if (tmp != NULL)	{
		// list not empty, so some work required
		*plisthead = tmp->next;	// list now starts with "second" node
		DestroyNode (tmp);
		}
}//RemoveNodeFromFront()


void RemoveNodeFromEnd (Node **plisthead)
// Remove last node from list
{
	Node	*tmp = *plisthead;	// point to first node in list

	if (tmp != NULL)	{
		// There's a list.  We find node that points to end (different from
		// how it was done in AddNodeToEnd() ).
		if (tmp->next == NULL)	{
			// first node is last (only) node
			*plisthead = NULL;	// list now empty
			DestroyNode (tmp);
		} else	{
			// more follow - keep traversing list until we find a node
			// that points to a node with a NULL next pointer
			while (tmp->next->next != NULL)	tmp = tmp->next;
			DestroyNode (tmp->next);
			tmp->next = NULL;
			}
		}
}//RemoveNodeFromEnd()


void AddAfterKey (Node **plisthead, Node *pnewnode, int ky)
// This function adds new node after node containing particular key value.
// Adding nodes to list depends on application.  Sometimes, lists only need
// to be added to their front and/or their end.  Other applications may want
// to add somewhere in between.  This function finds a node with a particular
// key value and adds the new node after it.
{
	Node	*tmp = *plisthead;	// point to first node in list

	if (tmp != NULL)	{
		// Non-empty list.  Find a node with matching key value
		// (or find end of list!)
		while ((tmp->key != ky)  && (tmp->next != NULL))
			tmp = tmp->next;
		// Got here because either we found node with matching key value,
		// or we came to end.
		if (tmp->key == ky)	{
			// Found node with matching key value
			// New node has to point to list after this node.
			pnewnode->next = tmp->next;
			// Node with matching value has to point to new node
			tmp->next = pnewnode;
			}
		}
}//AddAfterKey()


void DiscardList (Node **plisthead)
// Delete entire linked list - basically by repeatedly deleting first Node
{
	while(!isEmptyList(*plisthead))	{
		RemoveNodeFromFront(plisthead);
		}
}//DiscardList()




int main ()
{
	Node	*listhead1 = NULL;		// pointer to head of list
	Node	*listhead2 = NULL;		// A second list
	Node	*ptmp;					// temporary pointer to node
	int		i;
    
	PrintAllNodeData (listhead1);
	printf ("ADD 1 NODE\n");
	ptmp = CreateNode (1, 11, NULL);
	AddNodeToFront (&listhead1, ptmp);
	PrintAllNodeData (listhead1);

	printf ("ADD (2,12) THEN (3,13) TO FRONT\n");
	AddNodeToFront (&listhead1, CreateNode (2, 12, NULL));
	AddNodeToFront (&listhead1, CreateNode (3, 13, NULL));
	PrintAllNodeData (listhead1);

	printf ("ADD (4,14) THEN (5,15) TO END\n");
	AddNodeToEnd (&listhead1, CreateNode (4, 14, NULL));
	AddNodeToEnd (&listhead1, CreateNode (5, 15, NULL));
	PrintAllNodeData (listhead1);

	printf ("DELETE FIRST NODE\n");
	RemoveNodeFromFront (&listhead1);
	PrintAllNodeData (listhead1);

	printf ("DELETE LAST NODE\n");
	RemoveNodeFromEnd (&listhead1);
	PrintAllNodeData (listhead1);

	printf ("NOW ADD NODE AFTER FIRST ONE\n");
	AddAfterKey (&listhead1, CreateNode (9,19,NULL), 2);
	PrintAllNodeData (listhead1);

	DiscardList(&listhead1);
    return 0;
}
