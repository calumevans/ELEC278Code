// LNKLST_Better.C [160928]
//
// Code provided for ELEC278
//
// This code implements linked lists. It improves on LNKLST - by using
// a structure to describe the linked list, and eliminating the need to
// use double-pointer parameters in some of the functions.
//
// It is also improved in that each function explicitly checks the validity of
// the parameters passed to it. Getting into the habit of ALWAYS checking
// function parameters is a good way to eliminate a lot of bugs in software. 
//
// This implementation is similar to that used for Lab2.
//
// Note that the interface - the method of accessing the features of a linked
// list - is similar to the LNKLST version (same function names, but changes to
// parameters).

/* * START LICENSE
Code developed for educational purposes only.

Copyright 2016, 2017 by
David F. Athersych, Kingston, Ontario, Canada. (THE AUTHOR).
This software may be included in systems delivered or distributed by
Cynosure Computer Technologies Incorporated, Kingston, Ontario, Canada.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted, provided
that the above copyright notice appears in all copies and that both the
above copyright notice and this permission notice appear in supporting
documentation.  This software is made available "as is", and

THE AUTHOR DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, WITH REGARD
TO THIS SOFTWARE, INCLUDING WITHOUT LIMITATION ALL IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND IN NO EVENT
SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN AN ACTION OF CONTRACT, TORT (INCLUDING NEGLIGENCE)
OR STRICT LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

For more information, see www.cynosurecomputer.ca
* END LICENSE */

#include <stdio.h>
#include <stdlib.h>


//=============================================================================
// Node specific structure definition.  Node contaions actual data but also
// contains some structural information - a pointer to the next node.
//=============================================================================

// Node (or element) in list holds value and pointer to next element.
// Definition of structure comes first; typedef makes typing easier.
struct node {
	struct node	*next;
	int			key;
	int			value;
};
typedef struct node	Node;


Node *CreateNode (int ky, int val, Node *nxt)
// Create new node structure and initialize fields.
// Returns pointer to new node created, or NULL if malloc() failed to
// find memory.
{
	Node *pNode = (Node *)malloc (sizeof(Node));
	if (pNode != NULL)	{
		pNode->next = nxt;
		pNode->key = ky;
		pNode->value = val;
		}
	return pNode;
}//createNode()


void DestroyNode (Node *pnode)
// Basically a pretty way of using free()
{
	free (pnode);
}//DestroyNode()


void PrintNode (Node *pnode)
// Node-specific display routine. Given a pointer to a node, it prints actual
// data.
{
	// always check to make sure parameter valid, because sometimes errors
	// happen.
	if (pnode != NULL)	{
		printf ("key: %d, value: %d\n", pnode->key, pnode->value);
	} else	{
		printf ("ERROR: PrintNode() - invalid Node pointer\n");
		}
}//PrintNode()

//=============================================================================
// End of node-specific code
//=============================================================================


//=============================================================================
// Linked-list specific code
//=============================================================================

// Linked List structure. In LNKLST, there was no explicit list type - a list
// was defined by having a pointer to a first node.  Here, we have a specific
// list variable - which is separate from the list data to which it points.
// An instance of a struct lnkdlist is sometimes called a descriptor (as
// "linked list descriptor").

struct lnkdlist	{
	int		count;	// keep count of how many items in list
	Node	*llhed;	// point to first element in list
	Node	*llend;	// point to last element in list
};
typedef struct lnkdlist	LL;


struct lnkdlist *CreateList (void)
// Create new linked list descriptor
{
	LL	*tmp;		// pointer to new linked list structure
	tmp = (LL *)malloc (sizeof (struct lnkdlist));
					// get memory for a lnkdlist structure
	if (tmp != NULL)	{
		// initialize fields in linked list descriptor
		tmp->count = 0;
		tmp->llhed = NULL;
		tmp->llend = NULL;
		}
	// Return pointer to initialized linked list structure or return NULL
	// if malloc() couldn't get memory.
	return tmp;
}//CreateList()

void DestroyList(LL *plist)
// Destroy all memory used by linked list. This means destroying all nodes
// in the linked list and then destroying the linked list structure.
{
	if (plist != NULL)	{
		Node	*tmp;			// used to traverse linked list
		tmp = plist->llhed;		// point to first node in linked list
		while (tmp != NULL)	{
			Node	*tmp2 = tmp->next;	// save pointer to next node in list
			DestroyNode (tmp);			// destroy current node
			tmp = tmp2;					// move to next node
			}
		// All nodes destroyed - destroy linked list structure
		free (plist);
		}
}//DestroyList()

#if 0
// The one problem with the DestroyList code above is that the pointer to
// the linked list still points to where the linked list was. We would really
// like the pointer to the linked list structure zeroed as well. The code
// below does that, but it has one of the dreaded pointers to a pointer.

void DestroyList(LL **pplist)
// Destroy all memory used by linked list. This means destroying all nodes
// in the linked list and then destroying the linked list structure.
// Parameter pplist points to the pointer to the linked list.
{
	if (pplist != NULL)	{
		LL		*plist;			// pointer to the linked list structure
		Node	*tmp;			// used to traverse linked list
		plist = *pplist;		// get actual linked list pointer
		tmp = plist->llhed;		// point to first node in linked list
		while (tmp != NULL)	{
			Node	*tmp2 = tmp->next;	// save pointer to next node in list
			DestroyNode (tmp);			// destroy current node
			tmp = tmp2;					// move to next node
			}
		// All nodes destroyed - destroy linked list structure
		free (plist);
		*pplist = NULL;			// this will zero the actual linked list
								// structure pointer, so there is now no
								// way to accidentally try to access the
								// memory just free()ed.
		}
}//DestroyList()

#endif
		

// Linked List Utility Routines. Functions that operate on whole linked list.

int isEmptyList (LL *list)
// Empty list detected either by count of 0 or by llhed==llend==NULL.
{
	return  (list==NULL) || (list->count == 0);
}//isEmptyList()

#if 0
// Before moving on, consider alternate way of implementing isEmptyList().
// In the previous code, a pointer to LL was passed.  Thus, the total
// memory passed as parameter is enough to hold 1 pointer (4 bytes on a 32
// bit machine). Programmer has to pass a reference to the linked list
// descriptor. But what if we just passed the whole linked list descriptor?

int isEmptyList (LL list)		// note NO star
// Empty list detected either by count of 0 or by llhed==llend==NULL.
{
	return  (list==NULL) || (list.count == 0);
}//isEmptyList()


// What's the big deal?  We didn't write a * before the list parameter in the
// function definition; we used a period (.) instead of an arrow ( -> ) to
// access the count field, and when this function is called, it is called with
// a descriptor as a parameter, not a pointer to a descriptor.
// It is the last difference that has concerned programmers in the past. In
// the first version, the quantity of data passed is one pointer to the
// descriptor (4 bytes assuming a 32 bit computer).  In the second version, a
// whole descriptor is passed - one integer and two pointers - 12 bytes
// assuming a 32 bit computer.  Not a big difference, maybe, but it takes
// three times as long to store 3 32-bit parameters as to store 1 32-bit
// parameter.  And if this is in some kind of look, then this extra time gets
// spent every loop.
// Why might this not matter?  Because compilers (really compiler writers) are
// much more clever now than they used to be. Compilers could analyse this
// code at compile time, and make decisions about how much data really needs
// to be passed.  It may be that the compiler doesn't even generate a function
// call - it could just generate the comparison code inline - eliminating
// not only unnecessary parameter data but also eliminating the function call
// and return overhead.
// Bottom line - write code that is easy for humans to understand and rely on
// the compiler to make code efficient.
#endif


void PrintAllNodeData(LL *plist)
// Print all node data for Nodes found in list pointed to by plist.
{
	if (plist != NULL)	{
		Node	*thisnode;			// local pointer to nodes
		thisnode = plist->llhed;	// point to first node in list
		if (thisnode != NULL)	{
			// Data to print
			while (thisnode != NULL)	{
				PrintNode (thisnode);
				thisnode = thisnode->next;
				}
		} else	{
			printf ("EMPTY LIST\n");
			}
		}
}//PrintAllNodeData()


void AddNodeToFront(LL *plist, Node *pnewnode)
// Code to add new node to front of existing list.
// Parameters:
//	plist - points to existing linked list
//	newnode - points to new node which is to be added (see createNode())
{
	if (plist != NULL && pnewnode != NULL)	{
		Node	*tmp;			// temporary pointer to first list node
		tmp = plist->llhed;		// point to current first node (may be NULL)
		// Node pointed to by newnode will become first node in list. Existing
		// list may have Nodes,in which case head of list points to first one.
		// Existing list may be empty, meaning head of list contains NULL.
		pnewnode->next = tmp;	// New node points to existing list
		// Now, this is different from the LNKLST implementation. In this
		// implementation, we are keeping track of both the beginning and the
		// end of the list.  It is possible that we are adding the first node
		// to the list, and thus, we have to update the end-of-list pointer as
		// well.
		if (tmp == NULL)	{
			// original list was empty, so we are adding first node.  It also
			// becomes last node.
			plist->llend = pnewnode;
			}
		plist->llhed = pnewnode;	// newnode becomes first node in list
		}
}//AddNodeToFront()	


void AddNodeToEnd (LL *plist, Node *pnewnode)
// Add new node to end of existing list.  In LNKLST, we had to search
// for the last node, in order to add one after it.  In the current
// implementation of linked list, we have both start and end pointers.
{
	if (plist != NULL  && pnewnode != NULL)	{
		Node	*tmp;		// temporary pointer to last node in list
		tmp = plist->llend;	// set to point to last in list
		if (tmp == NULL)	{
			// this is case where list is empty - new node becomes both
			// first and last
			plist->llhed = plist->llend = pnewnode;
		} else	{
			// this is case where list has at least one element - and thus
			// has a last element
			tmp->next = pnewnode;		// current last node points to new node
			plist->llend = pnewnode;	// last node now becomes the new node
			}
		}
}//AddNodeToEnd()


void RemoveNodeFromFront (LL *plist)
// Remove first node from list
{
	if (plist != NULL)	{
		Node	*tmp;		// temporary used to point to node to delete
		tmp = plist->llhed;	// point to first node (or perhaps NULL)
		if (tmp != NULL)	{
			// list not empty, so there is a first node to delete
			plist->llhed = tmp->next;	// head now "second" node in list
			if (plist->llhed == NULL)	{
				// There was only one node and we just unlinked it.  Better
				// update the endlist pointe
				plist->llend = NULL;
				}
			// Node pointed to by tmp no longer part of list
			DestroyNode (tmp);
			}
		}
}//RemoveNodeFromFront()


void RemoveNodeFromEnd (LL *plist)
// Remove last node from list. Now this turns out to be the one routine
// that isn't improved by keeping track of the end.  Why? Because to delete
// the last node, we need the pointer to the node BEFORE the last one, so
// we can record that node as the new last one.
{
	if (plist != NULL)	{
		Node	*tmp;			// will eventually point to the node to delete
		// Now, there may only be one element in the list, so deleting the
		// last node is deleting the only node
		if (plist->llhed == plist->llend)	{
			// two pointers are the same - means only one node - the one to
			// delete
			tmp = plist->llend;	// point to the only node
			plist->llhed = plist->llend = NULL;
			DestroyNode (tmp);
		} else	{
			// Now we have to traverse the linked list, looking for a node
			// that points to the last one.
			tmp = plist->llhed;	// point to first node in list
			// keep going until the next pointer of the node equals the
			// pointer to the last node.  When that happens, we have found
			// the second-last node.
			while (tmp->next != plist->llend)	{
				tmp = tmp->next;
				}
			// Found second-last node - pointed to by tmp.  Get rid of last
			// node - the one after this one.
			DestroyNode (tmp->next);
			tmp->next = NULL;		// no longer points to anyhing
			plist->llend = tmp;		// new end-of-list node
			}
		}
}//RemoveNodeFromEnd()


void AddAfterKey (LL *plist, Node *pnewnode, int ky)
// This function adds new node after node containing particular key value.
// Adding nodes to list depends on application.  Sometimes, lists only need
// to be added to their front and/or their end.  Other applications may want
// to add somewhere in between.  This function finds a node with a particular
// key value and adds the new node after it.
{
	if (plist != NULL && pnewnode != NULL)	{
		Node	*tmp = plist->llhed;	// point to first node in list
		if (tmp != NULL)	{
			// Non-empty list.  Find node with matching key value
			// (or find end of list!)
			while ((tmp != NULL) && (tmp->key != ky))
				tmp = tmp->next;
			// Got here because either we found node with matching key value,
			// or we came to end.
			if (tmp->key == ky)	{
				// Found node with matching key value
				// New node has to point to list after this node.
				pnewnode->next = tmp->next;
				// Node with matching value has to point to new node
				tmp->next = pnewnode;
				// What if node found was last in the list? New node has just
				// become last in the list
				if (tmp == plist->llend)	{
					// yes - it was the last one
					plist->llend = pnewnode;
					}
				}
			}
		}
}//AddAfterKey()





int main ()
{
	LL		*linklist1;			// pointer to a linked list
	int		i;
	Node	*ptmp;

	linklist1 = CreateList ();	// now we have a linked list descriptor
	PrintAllNodeData (linklist1);
	printf ("ADD 1 NODE\n");
	ptmp = CreateNode (1, 11, NULL);
	AddNodeToFront (linklist1, ptmp);
	PrintAllNodeData (linklist1);

	printf ("ADD (2,12) THEN (3,13) TO FRONT\n");
	AddNodeToFront (linklist1, CreateNode (2, 12, NULL));
	AddNodeToFront (linklist1, CreateNode (3, 13, NULL));
	PrintAllNodeData (linklist1);

	printf ("ADD (4,14) THEN (5,15) TO END\n");
	AddNodeToEnd (linklist1, CreateNode (4, 14, NULL));
	AddNodeToEnd (linklist1, CreateNode (5, 15, NULL));
	PrintAllNodeData (linklist1);

	printf ("DELETE FIRST NODE\n");
	RemoveNodeFromFront (linklist1);
	PrintAllNodeData (linklist1);

	printf ("DELETE LAST NODE\n");
	RemoveNodeFromEnd (linklist1);
	PrintAllNodeData (linklist1);

	printf ("NOW ADD NODE AFTER FIRST ONE\n");
	AddAfterKey (linklist1, CreateNode (9,19,NULL), 2);
	PrintAllNodeData (linklist1);

	DestroyList(linklist1);
    return 0;
}
