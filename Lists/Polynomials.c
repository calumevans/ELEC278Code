// Polypomials.c [171005]
// This code illustrates use of linked list to represent polynomial.
// This code is intended for illustrative purposes only.


#include	<stdio.h>

// ====================================================================
// C doesn't have an intrinsic power operator.  We could use the pow()
// function provided in the math library (math.h) but we want to
// simplify things by just having integer exponents. These two routines
// provide the functionality for integer powers of integers and floats
// (doubles).

int ipower (int n, int exp)
// Integer compute n to the power exp. This is the version that returns
// integer result, so it doesn't handle negative exponents.
{
	int		count;
	int		rslt = 1;	// lets us handle 0 exponent
	if (exp < 0)	rslt = -1;
	else
		for (count = 1; count <= exp; count++)	rslt = rslt * n;
	return rslt;
}//ipower()

double dipower (double x, int exp)
// Computes double to the power of (integer) exponent.  Exponent can
// be either positive or negative.
{
	int		isneg = (exp < 0);	// keep track if exponent negative
	double	rslt = 1.0;
	int		count;

	if (isneg)	exp = -exp;
	for (count = 1; count <= exp; count++)	rslt = rslt * x;
	if (isneg) rslt = 1/rslt;
	return rslt;
}//dipower()

#ifdef TESTPOWER
// For testing power functions.

int main()
{
	int		m = 16;
	double	d = 16.0;
	int		i;
	for (i=0; i<5; i++)
		printf ("::  %3d to the power %2d is: %6d\n",
					m, i, ipower(m,i));
	for (i=-3; i<5; i++)
		printf (">>  %6.1lf to the power %2d is: %14.6lf\n",
					d, i, dipower (d, i));
	return 0;
}

#endif

	

//=====================================================================


// Structure of term in polynomial.  Essentially, represents term
// of the form  mult * X ^ exp, where ^ means "to power of"
// in this context.
struct _polyterm	{
	struct _polyterm	*ptnext;	// link to next item in list
	int					exp;		// Exponent value
	double				mult;		// Factor value
	};
typedef struct _polyterm	Polyterm, *pPolyterm;
#define	szPolyterm	(sizeof (struct _polyterm))

Polyterm *newPolyterm (int expon, double multiplier)
// Create new Polyterm node. Returns NULL if no Polyterm created,
// or pointer to a Polyterm if malloc worked.  If Polyterm
// created, exponent and factor fields are initialized.
{
	Polyterm	*ptm = (Polyterm *)malloc(szPolyterm);
	if (ptm != NULL)	{
		ptm->ptnext = NULL;
		ptm->exp = expon;
		ptm->mult = multiplier;
		}
	return ptm;
}//newPolyterm()

// Shortcut for new Polyterm with fields initialized to 0
#define newPolyterm0	newPolyterm(0,0.0)


void freePolyterm (pPolyterm pptm)
// Free polyterm structure pointed to by pptm
{
	if (pptm != NULL)	{ free (pptm); }
}//freePolyterm()



// Structure of polynomial. Pretty minimal.
struct _poly	{
	Polyterm	*pt;		// pointer to list of terms
	};
typedef struct _poly	Polynomial;

#define szPolynomial	(sizeof(struct _poly))


Polynomial *newPolynomial (void)
// Create Polynomial.  Created with no terms.
{
	Polynomial *ppnm = (Polynomial *)malloc(szPolynomial);
	if (ppnm != NULL)	{
		ppnm->pt = NULL;
		}
	return ppnm;
}//newPolynomial()


double devalPolynomial (Polynomial *pn, double x)
// Evaluate polynomial for given value of x.
{
	double		rslt = 0.0;				// default answer
	Polyterm	*current = pn->pt;		// point to first term

	while (current != NULL)	{
		// compute value for this term
		double		term = (current->mult)*ipower(x, current->exp);
		rslt += term;
		current = current->ptnext;
		}
	return rslt;
}//ievalPolynomial()


#define	ABS(x)	((x>=0)?x:-x)

void PrintPolynomial (Polynomial *pn)
// Print a readable representation of the polynomial
{
	printf ("f(X) = ");
	if (pn == NULL)		printf ("NULL");
	else	{
		Polyterm	*pcurr = pn->pt;
		while (pcurr != NULL)	{
			// print this term
			printf (" %c", (pcurr->mult < 0) ? '-' : '+');
			printf ("%5.2lf X ^ %d", ABS(pcurr->mult), pcurr->exp);
			pcurr = pcurr->ptnext;
			}
		}
	putchar ('\n');
}//PrintPolynomial()


int addPolyterm (Polynomial *ppnm, Polyterm *pt)
// Add existing Polyterm to Polynomial.
{
	int		rslt = -1;			// default return value is error
	if (ppnm != NULL && pt != NULL)	{
		// Use two pointers to  walk through list determining where
		// to put new Polyterm. Start by getting pointer to pointer
		// to first term, and pointer to first term.
		Polyterm **previous = &ppnm->pt;
		Polyterm *current = ppnm->pt;
		// walk down list until end, or until current has lower
		// exponent. Note that if list is empty, we will not execute
		// loop, but advance to code that follows.
		while (current != NULL)	{
			if (current->exp == pt->exp)	{
				// new term has same exponent as existing term
				// treat as error.
				break;
				}
			if (current->exp < pt->exp)	{
				// new term has to go before existing one
				// make new one point to existing
				pt->ptnext = current;
				// Make pointer to current point to new one
				*previous = pt;
				// we're done.  Set status and break out of loop.
				rslt = 0;
				break;
				}
			// Get here when current exponent is greater than
			// exponent in new term.  Advance to next position.
			previous = &(current->ptnext);
			current = current->ptnext;
			}
		// At this point, loop terminated because current pointer
		// became NULL. The new node goes here.
		pt->ptnext = current;
		*previous = pt;
		rslt = 0;
		}//endif
	return rslt;
}//addPolyterm()



#ifdef TESTPOLYNOMIAL

int main ()
{
	double		val, x;
	int			i;
	// Set up polynomial
	Polynomial *ppoly = newPolynomial();
	// Set up terms
	Polyterm *p0 = newPolyterm (0,4.0);
	Polyterm *p1 = newPolyterm (1,3.0);
	Polyterm *p2 = newPolyterm (2,1.0);
	// Add terms to polynomial	
	addPolyterm (ppoly, p0);	
	addPolyterm (ppoly, p1);				
	addPolyterm (ppoly, p2);
	PrintPolynomial (ppoly);
	for (i=0; i<5; i++)	{
		x = i;
		val = devalPolynomial (ppoly, x);
		printf (" x = %lf,  f = %lf\n", x, val);
		}
	return 0;
}

#endif		


			


