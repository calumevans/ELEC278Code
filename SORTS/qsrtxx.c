// QUICKSORTINP [161114]
//
// Implements simple quicksort algorithm.  Data to be sorted is input.
// TBD

#include <stdlib.h>
#include <stdio.h>

typedef int		T;

#if 0
int data[] = {19, 11, 93, 42, 67, 18, 13, 48, 6, 7, 38, 27, 22, 12, 5};
int datasize = 15;
#endif
#if 0
int data[] = {81, 13, 92, 21, 87, 50, 19, 63, 96, 4, 72};
int datasize = 11;
#endif
#if 0
int data[] = {83, 16, 91, 21, 97, 55, 19, 63, 17, 59, 3};
int datasize = 11;
#endif
#if 1
int data[] = {53, 16, 88, 24, 92, 58, 21, 63, 12, 3, 59};
int datasize = 11;
#endif


void printarray(T *a, int size)
{
    int i;
    for (i=0; i < size; i++)	printf(" %2d", a[i]);
    putchar('\n');
}

void printarrowatposition (int posn)
{
	int	i;
	for (i=0; i<posn; i++)		printf ("   ");
	printf ("  V\n");
}


void swap (T *a, int g, int h)
{
	int  t = a[g];
	printf ("   Swap position %d (%d) with position %d (%d)\n",
			g, a[g], h, a[h]);
	a[g] = a[h];
	a[h] = t;
}


int partition (T  *a,  int left, int right)
{
	int	ll, rr, pivotval;
	ll=left+1;
	rr=right;
	pivotval = a[left];
	printf ("Partition around value in position %d - %d\n", left, a[left]);
    while (ll < rr)	{
		// if one on left less than pivot, leave it
		if(a[ll] <= pivotval)	{ ll++; continue;}
		// if one on right greater than pivot, leave it
        if(a[rr] >= pivotval)	{ rr--; continue;}
		// both left and right on wrong side - swap them
		swap(a, ll, rr);
       	}//endwhile
	// we stop when rr and ll collide. Place pivot value
	// such that everything to left is lesser and everything
	// to right is same or greater.
	if (a[ll] < pivotval)	{
		swap(a, ll, left);
	} else	{
		swap(a, --ll, left);
		}
	return ll;
}//partition()


void quicksort (T  *a,  int left, int right)
{
	printf ("\nQuicksort: left = %d   right = %d\n", left, right);
	if (left < right)    {
		int  pivotndx = partition (a, left, right);
		printf ("    Completed partition, pivot at: %d\n", pivotndx);
		printarrowatposition (pivotndx);
		printarray (data, datasize);
		quicksort (a, left, pivotndx-1);
		quicksort (a, pivotndx+1, right);
		}
} 


int main(void)
{
	printf ("Show operation of QUICKSORT\n");
    printf("Original array: \n");
    printarray(data, datasize);
 
    quicksort(data, 0, datasize-1);
 
    printf("\nSorted array: \n");
    printarray(data, datasize);
	getchar ();
    return 0;
}
