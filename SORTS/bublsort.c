// BUBLSORT.C [161111]
// Implement simple bubblesort.

#include <stdio.h>

typedef int	T;

void printarray (char *msg, T *a, int min, int max)
// Print contents of array from position min to position max
{
	int		i;
	if (a == NULL)	return;
	if (msg != NULL)	printf ("%s\n", msg);
	for (i=min; i<=max; i++) printf (" %3d", a[i]);
	putchar ('\n');
}//printarray()

void swap (T *a, int g, int h)
{
	int  t = a[g];
	printf ("Swapping elements at %d (%d) and at %d (%d)\n",g,a[g],h,a[h]);
	a[g] = a[h];
	a[h] = t;
}


int bubbleup (T *a, int min, int max)
// Bubble largest element to end of array.
// Returns number of swaps done.
{
	int swaps = 0;
	int	i;
	if (a == NULL)	return -1;
	if (max <= min)	return 0;
	printf ("Start another bubble up\n");
	for (i=min; i<max; i++)	{
		if (a[i] > a[i+1])	{
			// out of order - swap required
			swap (a, i, i+1);
			swaps++;
			}
		}
	return swaps;
}//bubble()

// Array containing initial data
#define	MAXINDEX	19			// how much actual data
int data [100]	= {
	44, 22,  3, 71, 16, 82,  1,  9, 99, 10,
	32, 18, 77, 28,  9, 17, 52, 61, 38, 12 };

int main (void)
{
	int	rslt, max;
	printarray ("Array before any work done:", data, 0, MAXINDEX);

	// Every time through loop, one more data item is placed correctly.
	for (max=MAXINDEX; max>0; max--)	{
		rslt = bubbleup (data, 0, max);
		printf ("Swaps done: %d\n", rslt);
		printarray ("After another bubble :", data, 0, MAXINDEX);
		// if no swaps were done, we're finished
		if (rslt == 0)	break;
		}
	printarray ("Final array:", data, 0, MAXINDEX);
	return 0;
}//main()


