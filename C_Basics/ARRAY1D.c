// ArrayAdr.C [160926]
// Simple program to prove that all array elements stored in adjacent
// locations.
// Also shows that addresses of globals are significantly different than
// addresses of locals.

#include <stdio.h>

int gint [5];

int main (void)
{
    int     i;
    int     lclint [4];

    for (i=0; i<5; i++)
        printf ("Address of integer %d in gint: %08x\n", i, &gint[i]);

    for (i=0; i<4; i++)
        printf ("Address of integer %d in lclint: %08x\n", i, &lclint[i]);

    return 0;
}
