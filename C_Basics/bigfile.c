// BIGFILE.C [161117]
// Program to produce a test file of random records, used to test code
// written for the tutorial assignment on November 15, 2016.

/*
Original problem statement:

You have a collection of data records stored in a file that is too big
to store in memory. Each record of data has a 28 character key associated
with it.
The items are stored in the file in no particular order. You are asked to
write code that will create a sorted version of the file.  You are told
that each record is between 1 and 3 megabytes in size, and that the first
two fields in each record are (1) the 28 byte key with no trailing NUL
character and (2) a 4 byte integer indicating the size of the record,
including the key and size fields.  Show your code.

*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int		lastwriteerror = 0;

int writerandom (FILE *fp, int size)
// write random data - size bytes - out to file
{
	unsigned char	ch, rch;
	int		i;
	printf ("Writing %d bytes of fake data\n", size);
	for (ch = ' ', i=0; i<size; i++, ch++)	{
		if (ch > 0x7E)	ch = ' ';
		rch = fputc (ch, fp);
		if (rch != ch)	{
			lastwriteerror = 3;	// error writing the padding
			return 0;
			}
		}
	return 1;
}

int write_one_record (FILE *fp)
// Write one record to already-opened output file.
{
	long int	number;
	int			size;
	char		buff [30];
	int			rslt;

	// Step 1. Generate 28 byte header.  I'm just making junk up here.
	number = rand ();
	sprintf (buff,"%14ld-ASDFG-QWERY-Z", number);
	// Step 2. Generate size for record (Maximum 2Mb)
	size = 1000000 + (rand() << 6);
	// Step 3. Write out fixed size information
	rslt = fwrite (buff, 1, 28, fp);
	if (rslt != 28)	{
		lastwriteerror = 1;		// fail to write key
		return 0;
		}
	rslt = fwrite (&size, sizeof (int), 1, fp);
	if (rslt != 1)	{
		lastwriteerror = 2;		// fail to write size
		return 0;
		}
	// Step 4. Write out (fake) data
	size -= 32;					// take into account data written already
	return writerandom (fp, size);
}


int main (int argc, char *argv[])
{
	int		numrecords;
	int		i;
	int		rslt;
	FILE	*fpowt;

	if (argc != 2)	{
		printf ("ERROR - need to specify number of records\n");
		return -1;
		}
	numrecords = atoi (argv[1]);
	printf ("Generate file of %d random records\n", numrecords);
	// seed random number generator
	srand (time(NULL));
	// open output file (binary mode because this is Windoze
	fpowt = fopen ("RANDOM.DAT", "wb");
	if (fpowt == NULL)	{
		printf ("ERROR - Failed to create output file\n");
		return -1;
		}

	for (i=0; i<numrecords; i++)	{
		rslt = write_one_record (fpowt);
		if (rslt == 0)	{
			printf ("Error %d when writing record %d\n", lastwriteerror, i);
			fclose (fpowt);
			return -1;
			}
		}
	fclose (fpowt);
	return 0;
}

	
