// INTFLOAT.C [161002]
//

#include <stdio.h>

// Simple way to use same space for an int or a float.
typedef union _if {
   int    i;
   float  f;
   } IntFloat;

int main (void)
{
	IntFloat    x;
    int         k;

	printf ("int size: %d   float size: %d\n", sizeof(int), sizeof(float));

    x.f  = 1.0;
    printf ("%20.14f   %11d\n",  x.f,  x.i);
    x.f  = 2.0;
    printf ("%20.14f   %11d\n",  x.f,  x.i);
    x.i  = 1;
    printf ("%20.14f   %11d\n",  x.f,  x.i);
    x.i  = 2;
    printf ("%20.14f   %11d\n",  x.f,  x.i);

	for (k = 0; k<10; k++)	{
        x.i  = 0x10000000 + k;
        printf ("%20.14f   %11d\n",  x.f,  x.i);
        }
    for (k = 0; k<10; k++)	{
        x.i  = 0x10100000 + k;
        printf ("%20.14f   %11d\n",  x.f,  x.i);
        }
    for (k = 0; k<10; k++)	{
        x.i  = 0x11000000 + k;
        printf ("%20.14f   %11d\n",  x.f,  x.i);
        }
    //x.i  = 2000000000;
    //printf ("%20.10f   %11d\n",  x.f,  x.i);

    return 0;
}
